export const TIMEFRAME_LENGTH = 6;

export const EPIC_DETAILS_CELL_WIDTH = 320;

export const TIMELINE_CELL_MIN_WIDTH = 180;

export const SCROLL_BAR_SIZE = 15;

export const TIMELINE_END_OFFSET_HALF = 17;

export const TIMELINE_END_OFFSET_FULL = 26;
